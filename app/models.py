from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Album(models.Model):
    name = models.CharField(max_length=200);
    image = models.FileField();
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.name

class Photo(models.Model):
    title = 'photo'
    grids = (('1','1'),('2','2'))
    image = models.FileField();
    pub_date = models.DateTimeField('date published')
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    grid = models.CharField(max_length=1,choices = grids,default = 1)

    def __str__(self):
        return self.title
