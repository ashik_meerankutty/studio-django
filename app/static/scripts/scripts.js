$(window).scroll(function () {
if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
} else {
    $('nav').removeClass('shrink');
}
});
$(document).ready(function(){
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:3,
            loop:false
        }
    }
})
});
$(function() {
$('a[href*="#"]:not([href="#"])').click(function() {
if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
  var target = $(this.hash);
  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
  if (target.length) {
    $('html, body').animate({
      scrollTop: target.offset().top
    }, 1250);
    return false;
  }
}
});
});
document.addEventListener('DOMContentLoaded', function(){
  Typed.new('.type', {
    strings: ["Hello.", "Welcome to riven"],
    typeSpeed: 150
  });
});

$('.main_slider').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
    dots:true,
    dotsEach:true,
    autoplay:true,
    items:1,
})

$(function() {
	$('.auto-height').matchHeight();
  $('.auto-height-2').matchHeight({
     byRow: 0 });
});
