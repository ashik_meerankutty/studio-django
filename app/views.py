from django.shortcuts import render
from django.http import HttpResponse
from .models import Album,Photo

# Create your views here.
def home(request):
    return render(request,'site/index.html')

def about(request):
    return render(request,'site/about.html')

def services(request):
    return render(request,'site/services.html')

def gallery(request):
    album = Album.objects.order_by('-pub_date')
    context = {'album' : album}
    return render(request,'site/portfolio.html',context)

def contact(request):
    return render(request,'site/contact.html')
def photos(request,Album_id):
    album = Album.objects.get(pk = Album_id)
    return render(request, 'site/gallery.html',{'album': album,})
