# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-06-21 15:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Images',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.FileField(upload_to=b'')),
                ('pub_date', models.DateTimeField(verbose_name='date published')),
                ('album', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Album')),
            ],
        ),
    ]
